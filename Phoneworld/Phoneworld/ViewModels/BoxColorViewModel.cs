﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Phoneworld.Annotations;
using Xamarin.Forms;

namespace Phoneworld.ViewModels
{
    public class BoxColorViewModel: INotifyPropertyChanged
    {
        private double _red, _blue, _green;
        private Color _color;

        public double Red
        {
            get { return _red; }
            set
            {
                if (_red != value)
                {
                    _red = value;
                    OnPropertyChanged();
                    SetColor();
                }
            }
        }

        public double Green
        {
            get
            {
                return _green;
            }
            set
            {
                if (_green != value)
                {
                    _green = value;
                    OnPropertyChanged();
                    SetColor();
                }
            }
        }

        public double Blue
        {
            get { return _blue; }
            set
            {
                if (_blue != value)
                {
                    _blue = value;
                    OnPropertyChanged();
                    SetColor();
                }
            }
        }

        public Color Color
        {
            get { return _color; }
            set
            {
                if (_color != value)
                {
                    _color = value;
                    OnPropertyChanged();
                    Red = _color.R;
                    Green = _color.G;
                    Blue = _color.B;
                }
            }
        }

        public void SetColor()
        {
            Color = new Color(Red, Green, Blue);
        }

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
