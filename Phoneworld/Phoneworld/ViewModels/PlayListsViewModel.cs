﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Phoneworld.Annotations;
using Xamarin.Forms;

namespace Phoneworld.ViewModels
{
    public class PlayListsViewModel : INotifyPropertyChanged
    {
        private PlayItemViewModel _selectePlayItem;
        
        public ObservableCollection<PlayItemViewModel> PlayItemList
        {
            get; set;
        } 

        public PlayItemViewModel SelectedPlayItem
        {
            get { return _selectePlayItem; }
            set
            {
                if (_selectePlayItem == value) return;
                _selectePlayItem = value;
                OnPropertyChanged();
            }
        }

        public ICommand AddPlayListCommand { get; private set; }
        public ICommand SelectPlayListCommand { get; private set; }

        public PlayListsViewModel()
        {
            PlayItemList = new ObservableCollection<PlayItemViewModel>();
            AddPlayListCommand = new Command(
                //command login
                () =>
                {
                    var newItem = new PlayItemViewModel
                    {
                        IsFavourite = false,
                        Title = $"Play List Item {PlayItemList.Count + 1}"
                    };
                    PlayItemList.Add(newItem);                    
                    ((Command)this.AddPlayListCommand).ChangeCanExecute();
                },
                //check whether exceed max items
                () =>
                {
                    var result = PlayItemList.Count <= 10;
                    return result;
                });

            SelectPlayListCommand = new Command<PlayItemViewModel>(
                (item) =>
                {
                    if (item == null) return;

                    SelectedPlayItem = item;
                    SelectedPlayItem.IsFavourite = !SelectedPlayItem.IsFavourite;
                });
        }
        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
