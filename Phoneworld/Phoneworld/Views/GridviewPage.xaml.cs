﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoneworld.ViewModels;
using Xamarin.Forms;

namespace Phoneworld.Views
{
    public partial class GridviewPage : ContentPage
    {
        public GridviewPage(BoxColorViewModel color)
        {
            InitializeComponent();
            BindingContext = color;
        }

        private void GridviewPage_OnSizeChanged(object sender, EventArgs e)
        {
            //Portrait mode
            if (Width < Height)
            {
                grdView.RowDefinitions[1].Height = GridLength.Auto;
                grdView.ColumnDefinitions[1].Width = new GridLength(0, GridUnitType.Absolute);
                Grid.SetRow(controlPanelStack, 1);
                Grid.SetColumn(controlPanelStack, 0);
            }
            //Landscape mode
            else
            {
                grdView.RowDefinitions[1].Height = new GridLength(0, GridUnitType.Absolute);
                grdView.ColumnDefinitions[1].Width = new GridLength(1, GridUnitType.Star);
                Grid.SetRow(controlPanelStack, 0);
                Grid.SetColumn(controlPanelStack, 1);
            }
        }

        
    }
}
