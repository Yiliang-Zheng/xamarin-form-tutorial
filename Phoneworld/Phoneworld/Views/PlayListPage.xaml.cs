﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoneworld.ViewModels;
using Xamarin.Forms;

namespace Phoneworld.Views
{
    public partial class PlayListPage : ContentPage
    {
        public PlayListPage()
        {
            ViewModel = new PlayListsViewModel();
            InitializeComponent();            
        }

        private void ListView_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            ViewModel.SelectPlayListCommand.Execute(e.SelectedItem);
        }

        public PlayListsViewModel ViewModel
        {
            get { return BindingContext as PlayListsViewModel; }
            set { BindingContext = value; }
        }
    }
}
