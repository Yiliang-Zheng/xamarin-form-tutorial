﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using Phoneworld.Annotations;
using Xamarin.Forms;

namespace Phoneworld.ViewModels
{
    public class PlayItemViewModel : INotifyPropertyChanged
    {
        private bool _isFavorite;
        public string Title { get; set; }

        public bool IsFavourite
        {
            get { return _isFavorite; }
            set
            {
                if (_isFavorite == value) return;
                _isFavorite = value;
                OnPropertyChanged();
                OnPropertyChanged(nameof(Color));
            }
        }

        public Color Color => IsFavourite ? Color.Pink : Color.Black;

        public event PropertyChangedEventHandler PropertyChanged;

        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
