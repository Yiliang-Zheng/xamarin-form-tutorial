﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoneworld.ViewModels;
using Xamarin.Forms;

namespace Phoneworld.Views
{
    public partial class MasterDetail : MasterDetailPage
    {
        //public ObservableCollection<PlayItemViewModel> Collection => new ObservableCollection<PlayItemViewModel>()
        //{
        //    new PlayItemViewModel
        //    {
        //        IsFavourite = true,
        //        Title = "Hello"
        //    }
        //};

        public MasterDetail()
        {
            InitializeComponent();
            //lstString.ItemsSource = NamedColor.All;
        }

        private void LstString_OnItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as NamedColor;
            if (item != null)
            {
                var boxedColor = new BoxColorViewModel
                {
                    Color = item.Color,
                    Green = item.Color.G,
                    Blue = item.Color.B,
                    Red = item.Color.R
                };
                Detail = new GridviewPage(boxedColor);
            }
            lstString.SelectedItem = null;
            IsPresented = false;
        }
    }
}
