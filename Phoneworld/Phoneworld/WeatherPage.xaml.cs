﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Phoneworld;
using Xamarin.Forms;

namespace WeatherApp
{
    public partial class WeatherPage : ContentPage
    {
        public WeatherPage()
        {
            InitializeComponent();
            this.Title = "Sample Weather App";
            getWeatherBtn.Clicked += GetWeatherBtn_Clicked;

            //Set the default binding to a default object for now  
            this.BindingContext = new Weather();
        }

        private async void GetWeatherBtn_Clicked(object sender, EventArgs e)
        {
            //Weather weather = await Core.GetWeather("60601");
            //getWeatherBtn.Text = "Melbourne";
            await Navigation.PushAsync(new Listview());
        }

        private async void BtnGetColor_OnClicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new CurrentTime());
        }
    }
}
